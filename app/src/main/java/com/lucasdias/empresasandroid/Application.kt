package com.lucasdias.empresasandroid

import android.app.Application
import com.lucasdias.companycatalog.di.companyCatalogModule
import com.lucasdias.companydetail.di.companyDetailModule
import com.lucasdias.log.LogApp
import com.lucasdias.login.di.loginModule
import org.koin.android.ext.koin.androidContext
import org.koin.android.logger.AndroidLogger
import org.koin.core.context.startKoin
import org.koin.core.logger.EmptyLogger
import org.koin.core.logger.Logger

class Application : Application() {

    override fun onCreate() {
        super.onCreate()
        LogApp.initialize(BuildConfig.DEBUG)

        startKoin {
            androidContext(this@Application)
            logger(koinLogger())
            modules(
                listOf(
                    loginModule,
                    companyCatalogModule,
                    companyDetailModule
                )
            )
        }
    }

    private fun koinLogger(): Logger = if (BuildConfig.DEBUG) AndroidLogger() else EmptyLogger()
}
