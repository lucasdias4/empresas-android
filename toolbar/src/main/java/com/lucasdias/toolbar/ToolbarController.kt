package com.lucasdias.toolbar

import android.app.Activity
import android.content.res.ColorStateList
import android.os.Build
import android.view.KeyEvent
import android.view.View
import android.widget.FrameLayout
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.ActionBar
import androidx.core.content.ContextCompat
import com.google.android.material.appbar.AppBarLayout
import com.google.android.material.textfield.TextInputEditText
import com.google.android.material.textfield.TextInputLayout
import com.lucasdias.extensions.gone
import com.lucasdias.extensions.visible

/**
 * Este módulo tem como objetivo ser uma biblioteca. Logo, seus métodos existem independente
 * de serem utilizados pelo client, já que a lib não tem noção do contexto externo.
 **/

class ToolbarController(
    private var activity: Activity,
    toolbarContainer: FrameLayout,
    supportActionBar: ActionBar?
) {

    private companion object {
        const val NO_PADDING = 0
        const val NONE = 0
        const val EMPTY_STRING = ""
    }

    private var toolbarView: View
    private var toolbarLayout: AppBarLayout
    private var toolbarIcon: ImageView
    private var toolbarTitle: TextView
    private var toolbarBackButton: ImageView
    private var toolbarFrontView: FrameLayout
    private var toolbarInputText: TextInputEditText
    private var toolbarInputTextWrapper: TextInputLayout

    init {
        supportActionBar?.hide()
        val inflater = activity.layoutInflater

        toolbarView = inflater.inflate(R.layout.toolbar_layout, null)
        toolbarLayout = toolbarView.findViewById(R.id.app_bar_layout_toolbar_layout)
        toolbarIcon = toolbarView.findViewById(R.id.icon_toolbar_content_layout)
        toolbarTitle = toolbarView.findViewById(R.id.title_toolbar_content_layout)
        toolbarBackButton = toolbarView.findViewById(R.id.back_button_toolbar_content_layout)
        toolbarFrontView = toolbarView.findViewById(R.id.front_view_toolbar_content_layout)
        toolbarInputText = toolbarView.findViewById(R.id.text_input_toolbar_content_layout)
        toolbarInputTextWrapper = toolbarView.findViewById(R.id.wrapper_text_input_toolbar_content_layout)
        toolbarContainer.addView(toolbarView, 0)
    }

    fun setToolbarColor(toolbarColor: Int) {
        val color = ContextCompat.getColor(activity, toolbarColor)
        toolbarLayout.setBackgroundColor(color)
    }

    fun setToolbarBackButton() {
        val toolbarIconPaddingLeft =
            activity
                .resources
                .getDimension(
                    R.dimen.icon_padding_start_with_back_button_toolbar_content_layout
                )
                .toInt()

        toolbarIcon.setPadding(toolbarIconPaddingLeft, NO_PADDING, NO_PADDING, NO_PADDING)
        backButtonClickSetup(toolbarBackButton = toolbarBackButton)
        toolbarBackButton.visible()
    }

    fun backButtonClickSetup(toolbarBackButton: ImageView) {
        toolbarBackButton.setOnClickListener {
            activity.finish()
        }
    }

    fun removeToolbarBackButton() {
        toolbarBackButton.setPadding(NO_PADDING, NO_PADDING, NO_PADDING, NO_PADDING)
        toolbarBackButton.gone()
    }

    fun setTextInput(doASearchMethod: (String) -> Unit) {
        toolbarInputText.visible()
        toolbarInputTextWrapper.visible()
        setupTextInputKeyboard(doASearchMethod = doASearchMethod)
    }

    fun removeTextInput() {
        toolbarInputText.gone()
    }

    fun setTextInputHintText(hintText: String) {
        toolbarInputText.hint = hintText
    }

    fun setTextInputHintColor(hintColor: Int) {
        val color = ContextCompat.getColor(activity, hintColor)
        toolbarInputText.setHintTextColor(color)
    }

    fun setTextInputTextColor(textColor: Int) {
        val color = ContextCompat.getColor(activity, textColor)
        toolbarInputText.setTextColor(color)
    }

    fun setTextInputLineColor(lineColor: Int) {
        val color = ContextCompat.getColor(activity, lineColor)
        toolbarInputText.backgroundTintList = ColorStateList.valueOf(color)
    }

    fun setTextInputLeftIcon(icon: Int) {
        toolbarInputText.setCompoundDrawablesWithIntrinsicBounds(icon, NONE, NONE, NONE)
    }

    fun setToolbarFrontView(view: View) {
        toolbarFrontView.addView(view)
        showToolbarFrontView()

        toolbarFrontView.setOnClickListener {
            removeToolbarFrontView()
        }
    }

    fun showToolbarFrontView() {
        toolbarFrontView.visible()
    }

    fun removeToolbarFrontView() {
        toolbarFrontView.gone()
    }

    private fun setupTextInputKeyboard(doASearchMethod: (String) -> Unit) {
        toolbarInputText.setOnKeyListener { _, keyCode, event ->
            val searchText = toolbarInputText.text ?: EMPTY_STRING
            val userHitEnterButton =
                (event?.action == KeyEvent.ACTION_DOWN) && (keyCode == KeyEvent.KEYCODE_ENTER)

            if (userHitEnterButton) {
                doASearchMethod(searchText.toString())
                return@setOnKeyListener true
            }
            return@setOnKeyListener false
        }
    }
}
