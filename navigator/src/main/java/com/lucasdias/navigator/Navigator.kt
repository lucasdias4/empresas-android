package com.lucasdias.navigator

import android.content.Context
import android.content.Intent
import android.os.Bundle

class Navigator {

    companion object {

        fun navigateTo(
            context: Context,
            destiny: Activities,
            bundle: Bundle = Bundle()
        ) {
            val intent = Intent(context, Class.forName(destiny.address))
            intent.putExtras(bundle)
            context.startActivity(intent)
        }
    }

    enum class Activities(val address: String) {
        LOGIN(address = "com.lucasdias.login.presentation.LoginActivity"),
        COMPANY_CATALOG(address = "com.lucasdias.companycatalog.presentation.CompanyCatalogActivity"),
        COMPANY_DETAIL(address = "com.lucasdias.companydetail.presentation.CompanyDetailActivity")
    }
}
