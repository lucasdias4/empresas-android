package com.lucasdias.login.di

import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import com.lucasdias.connectivity.Connectivity
import com.lucasdias.login.BuildConfig.API_URL
import com.lucasdias.login.data.LoginRepositoryImpl
import com.lucasdias.login.data.remote.LoginService
import com.lucasdias.login.domain.repository.LoginRepository
import com.lucasdias.login.domain.usecase.PerformInvestorLoginOnTheWebServer
import com.lucasdias.login.presentation.LoginViewModel
import java.util.concurrent.TimeUnit
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.android.ext.koin.androidApplication
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.core.qualifier.named
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

internal const val LOGIN_CONNECTIVITY = "LOGIN_CONNECTIVITY"
private const val LOGIN_RETROFIT = "LOGIN_RETROFIT"
private const val LOGIN_OKHTTP = "LOGIN_OKHTTP"

@Suppress("RemoveExplicitTypeArguments", "USELESS_CAST")
val loginModule = module {

    viewModel {
        LoginViewModel(get<PerformInvestorLoginOnTheWebServer>())
    }

    factory(named(LOGIN_CONNECTIVITY)) {
        Connectivity(
            application = androidApplication()
        )
    }

    factory {
        PerformInvestorLoginOnTheWebServer(
            get<LoginRepository>()
        )
    }

    factory {
        LoginRepositoryImpl(
            get<LoginService>()
        ) as LoginRepository
    }

    // Service
    factory(named(LOGIN_OKHTTP)) {
        createOkHttpClient()
    }

    single(named(LOGIN_RETROFIT)) {
        createRetrofit(
            get<OkHttpClient>(named(LOGIN_OKHTTP))
        )
    }

    factory {
        getLoginService(
            get<Retrofit>(named(LOGIN_RETROFIT))
        )
    }
}

private fun createOkHttpClient(): OkHttpClient {
    val timeoutInSeconds = 10L
    val httpLoggingInterceptor = HttpLoggingInterceptor()
    httpLoggingInterceptor.level = HttpLoggingInterceptor.Level.BASIC
    return OkHttpClient.Builder()
        .connectTimeout(timeoutInSeconds, TimeUnit.SECONDS)
        .readTimeout(timeoutInSeconds, TimeUnit.SECONDS)
        .addInterceptor(httpLoggingInterceptor).build()
}

private fun createRetrofit(okHttpClient: OkHttpClient): Retrofit = Retrofit.Builder()
    .baseUrl(API_URL)
    .client(okHttpClient)
    .addConverterFactory(GsonConverterFactory.create())
    .addCallAdapterFactory(CoroutineCallAdapterFactory())
    .build()

private fun getLoginService(retrofit: Retrofit): LoginService =
    retrofit.create(LoginService::class.java)
