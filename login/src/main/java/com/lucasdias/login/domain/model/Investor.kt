package com.lucasdias.login.domain.model

data class Investor(
    var id: Long,
    var investorName: String,
    var email: String,
    var uid: String,
    var accessToken: String,
    var client: String
)
