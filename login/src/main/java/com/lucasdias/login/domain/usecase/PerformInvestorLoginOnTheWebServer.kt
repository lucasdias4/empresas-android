package com.lucasdias.login.domain.usecase

import com.lucasdias.login.domain.repository.LoginRepository

class PerformInvestorLoginOnTheWebServer(
    private val loginRepository: LoginRepository
) {
    suspend operator fun invoke(email: String, password: String) =
        loginRepository.performInvestorLoginOnTheWebServer(email = email, password = password)
}
