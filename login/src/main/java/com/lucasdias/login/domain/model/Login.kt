package com.lucasdias.login.domain.model

import com.lucasdias.login.domain.sealedclass.RequestStatus

data class Login(
    var requestStatus: RequestStatus,
    var investor: Investor?
)
