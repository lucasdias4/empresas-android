package com.lucasdias.login.domain.sealedclass

sealed class RequestStatus

object Success : RequestStatus()
object Error : RequestStatus()
object Unauthorized : RequestStatus()
