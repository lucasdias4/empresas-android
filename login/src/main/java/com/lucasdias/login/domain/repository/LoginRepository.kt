package com.lucasdias.login.domain.repository

import com.lucasdias.login.domain.model.Login

interface LoginRepository {
    suspend fun performInvestorLoginOnTheWebServer(email: String, password: String): Login
}
