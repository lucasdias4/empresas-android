package com.lucasdias.login.data.remote.response

data class LoginResponse(
    var investor: InvestorResponse,
    var portfolio: PortfolioResponse,
    var enterprise: Any?,
    var success: Boolean
)
