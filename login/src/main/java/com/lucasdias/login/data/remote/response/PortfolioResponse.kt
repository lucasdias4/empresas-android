package com.lucasdias.login.data.remote.response

import com.google.gson.annotations.SerializedName

data class PortfolioResponse(
    @SerializedName("enterprises_number") var enterprisesNumber: Int,
    var enterprises: ArrayList<String>
)
