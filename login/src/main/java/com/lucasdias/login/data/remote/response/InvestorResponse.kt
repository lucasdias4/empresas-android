package com.lucasdias.login.data.remote.response

import com.google.gson.annotations.SerializedName

data class InvestorResponse(
    var id: Long,
    @SerializedName("investor_name") var name: String,
    var email: String,
    var city: String,
    var country: String,
    var balance: Float,
    var photo: String,
    var portfolio: PortfolioResponse,
    @SerializedName("portfolio_value") var portifolioValue: Float,
    @SerializedName("first_access") var firstAccess: Boolean,
    @SerializedName("super_angel") var superAngel: Boolean
)
