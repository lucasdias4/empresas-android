package com.lucasdias.login.data.mapper

import com.lucasdias.login.data.remote.response.LoginResponse
import com.lucasdias.login.domain.model.Investor
import com.lucasdias.login.domain.model.Login
import com.lucasdias.login.domain.sealedclass.RequestStatus

class LoginMapper {
    companion object {
        fun map(
            loginResponse: LoginResponse?,
            accessToken: String,
            client: String,
            uid: String,
            status: RequestStatus
        ): Login {
            var investor: Investor? = null
            loginResponse?.investor?.let { investorResponse ->
                investor = Investor(
                    id = investorResponse.id,
                    investorName = investorResponse.name,
                    email = investorResponse.email,
                    uid = uid,
                    accessToken = accessToken,
                    client = client
                )
            }
            return Login(requestStatus = status, investor = investor)
        }
    }
}
