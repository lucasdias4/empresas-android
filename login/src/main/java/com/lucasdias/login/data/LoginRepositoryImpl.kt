package com.lucasdias.login.data

import com.github.kittinunf.result.coroutines.SuspendableResult
import com.lucasdias.log.LogApp
import com.lucasdias.login.data.mapper.LoginMapper
import com.lucasdias.login.data.remote.LoginService
import com.lucasdias.login.data.remote.response.LoginResponse
import com.lucasdias.login.domain.model.Login
import com.lucasdias.login.domain.repository.LoginRepository
import com.lucasdias.login.domain.sealedclass.Error
import com.lucasdias.login.domain.sealedclass.RequestStatus
import com.lucasdias.login.domain.sealedclass.Success
import com.lucasdias.login.domain.sealedclass.Unauthorized
import retrofit2.Response

class LoginRepositoryImpl(
    private val loginService: LoginService
) : LoginRepository {

    private companion object {
        const val MIN_RESPONSE_CODE = 200
        const val MAX_RESPONSE_CODE = 299
        const val UNAUTHORIZED_RESPONSE_CODE = 401
        const val ACCESS_TOKEN_KEY = "access-token"
        const val CLIENT_KEY = "client"
        const val UID_KEY = "uid"
        const val EMPTY_STRING = ""
    }

    override suspend fun performInvestorLoginOnTheWebServer(
        email: String,
        password: String
    ): Login {
        val result: SuspendableResult<Response<LoginResponse>, Exception> =
            SuspendableResult.of {
                loginService.performInvestorLoginOnTheWebServer(
                    email = email,
                    password = password
                ).await()
            }

        val resultCode = result.component1()?.code()
        val resultException = result.component2()
        val status = resultStatusHandler(
            resultCode = resultCode,
            resultException = resultException
        )

        val login: Login = if (status == Success) {
            onSuccess(status = status, response = result.component1())
        } else {
            onError(status = status, resultCode = resultCode, exception = result.component2())
        }
        return login
    }

    private fun resultStatusHandler(
        resultCode: Int?,
        resultException: java.lang.Exception?
    ): RequestStatus {
        when (resultCode) {
            in MIN_RESPONSE_CODE..MAX_RESPONSE_CODE -> {
                val isAnException = resultException != null
                if (isAnException) {
                    return Error
                }
                return Success
            }
            UNAUTHORIZED_RESPONSE_CODE -> {
                return Unauthorized
            }
            else -> {
                return Error
            }
        }
    }

    private fun onSuccess(
        status: RequestStatus,
        response: Response<LoginResponse>?
    ): Login {
        val accessToken: String = response?.headers()?.get(ACCESS_TOKEN_KEY) ?: EMPTY_STRING
        val client: String = response?.headers()?.get(CLIENT_KEY) ?: EMPTY_STRING
        val uid: String = response?.headers()?.get(UID_KEY) ?: EMPTY_STRING
        val loginResponse = response?.body()

        val login = LoginMapper.map(
            loginResponse = loginResponse,
            accessToken = accessToken,
            client = client,
            uid = uid,
            status = status
        )

        logRequestInfo(login = login)
        return login
    }

    private fun onError(
        status: RequestStatus,
        resultCode: Int?,
        exception: Exception?
    ): Login {
        val login = Login(requestStatus = status, investor = null)
        logRequestException(exception = exception, resultCode = resultCode)
        return login
    }

    private fun logRequestInfo(login: Login) {
        LogApp.i("LoginModule", "Request response START ---------->")
        LogApp.i("LoginModule", "Request status: ${login.requestStatus}")
        LogApp.i(
            "LoginModule", " \nid: ${login.investor?.id}" +
                    "\ninvestorName: ${login.investor?.investorName}" +
                    "\ne-mail: ${login.investor?.email}" +
                    "\nuid: ${login.investor?.uid}" +
                    "\naccessToken: ${login.investor?.accessToken}" +
                    "\nclient: ${login.investor?.client}"
        )
        LogApp.i("LoginModule", "Request response END <----------")
    }

    private fun logRequestException(exception: java.lang.Exception?, resultCode: Int?) {
        LogApp.i("LoginModule", "Request exception START ---------->")
        LogApp.i("LoginModule", "Exception or ErrorCode: ${exception ?: resultCode}")
        LogApp.i("LoginModule", "Request exception END <----------")
    }
}
