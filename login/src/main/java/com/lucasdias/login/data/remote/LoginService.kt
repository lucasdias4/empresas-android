package com.lucasdias.login.data.remote

import com.lucasdias.login.data.remote.response.LoginResponse
import kotlinx.coroutines.Deferred
import retrofit2.Response
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.POST

interface LoginService {
    @FormUrlEncoded
    @POST("users/auth/sign_in")
    fun performInvestorLoginOnTheWebServer(
        @Field(value = "email", encoded = false) email: String,
        @Field(value = "password") password: String
    ): Deferred<Response<LoginResponse>>
}
