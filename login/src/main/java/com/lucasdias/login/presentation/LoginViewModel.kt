package com.lucasdias.login.presentation

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.lucasdias.login.domain.model.Investor
import com.lucasdias.login.domain.sealedclass.Error
import com.lucasdias.login.domain.sealedclass.Success
import com.lucasdias.login.domain.sealedclass.Unauthorized
import com.lucasdias.login.domain.usecase.PerformInvestorLoginOnTheWebServer
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class LoginViewModel(
    internal val performInvestorLoginOnTheWebServer: PerformInvestorLoginOnTheWebServer
) : ViewModel() {

    internal var coroutineContext = Dispatchers.IO
    internal var wasConnected = true
    internal var investor = MutableLiveData<Investor>()
    internal var showConnectivitySnackbar = MutableLiveData<Unit>()
    internal var hideConnectivitySnackbar = MutableLiveData<Unit>()
    internal var errorRequestStatus = MutableLiveData<Unit>()
    internal var unauthorizedRequestStatus = MutableLiveData<Unit>()
    internal var userNeedsToFillInThePassword = MutableLiveData<Unit>()
    internal var userNeedsToFillInTheEmail = MutableLiveData<Unit>()
    internal var turnOnLoadingMode = MutableLiveData<Unit>()
    internal var turnOffLoadingMode = MutableLiveData<Unit>()

    fun getInvestor(): LiveData<Investor> = investor
    fun errorRequestStatus(): LiveData<Unit> = errorRequestStatus
    fun unauthorizedRequestStatus(): LiveData<Unit> = unauthorizedRequestStatus
    fun userNeedsToFillInThePassword(): LiveData<Unit> = userNeedsToFillInThePassword
    fun userNeedsToFillInTheEmail(): LiveData<Unit> = userNeedsToFillInTheEmail
    fun turnOnLoadingMode(): LiveData<Unit> = turnOnLoadingMode
    fun turnOffLoadingMode(): LiveData<Unit> = turnOffLoadingMode
    fun showConnectivitySnackbar(): LiveData<Unit> = showConnectivitySnackbar
    fun hideConnectivitySnackbar(): LiveData<Unit> = hideConnectivitySnackbar

    fun userWantsToLogin(email: String, password: String) {
        if (wasConnected.not()) return

        if (email.isEmpty()) {
            userNeedsToFillInTheEmail.postValue(Unit)
            return
        }

        if (password.isEmpty()) {
            userNeedsToFillInThePassword.postValue(Unit)
            return
        }

        performInvestorLogin(email, password)
    }

    fun mustShowConnectivitySnackbar(hasNetworkConnectivity: Boolean) {
        if (hasNetworkConnectivity.not()) {
            hideConnectivitySnackbar.postValue(Unit)
            wasConnected = false
        } else if (wasConnected.not() && hasNetworkConnectivity) {
            showConnectivitySnackbar.postValue(Unit)
            wasConnected = true
        }
    }

    internal fun performInvestorLogin(email: String, password: String) {
        CoroutineScope(coroutineContext).launch {
            turnOnLoadingMode.postValue(Unit)
            val login = performInvestorLoginOnTheWebServer(
                email = email,
                password = password
            )

            turnOffLoadingMode.postValue(Unit)

            when (login.requestStatus) {
                Success -> investor.postValue(login.investor)
                Unauthorized -> unauthorizedRequestStatus.postValue(Unit)
                Error -> errorRequestStatus.postValue(Unit)
            }
        }
    }
}
