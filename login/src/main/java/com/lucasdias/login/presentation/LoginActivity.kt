package com.lucasdias.login.presentation

import android.app.Activity
import android.os.Bundle
import android.view.View
import android.view.View.OnFocusChangeListener
import android.view.inputmethod.InputMethodManager
import android.widget.Button
import android.widget.ProgressBar
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import com.google.android.material.snackbar.Snackbar
import com.google.android.material.textfield.TextInputEditText
import com.lucasdias.connectivity.Connectivity
import com.lucasdias.extensions.bind
import com.lucasdias.extensions.gone
import com.lucasdias.extensions.toast
import com.lucasdias.extensions.visible
import com.lucasdias.login.R
import com.lucasdias.login.di.LOGIN_CONNECTIVITY
import com.lucasdias.login.domain.model.Investor
import com.lucasdias.navigator.Navigator
import org.koin.android.ext.android.inject
import org.koin.android.viewmodel.ext.android.viewModel
import org.koin.core.qualifier.named

class LoginActivity : AppCompatActivity() {

    companion object {
        private const val EMPTY_STRING = ""
        private const val UID_KEY = "UID_KEY"
        private const val CLIENT_KEY = "CLIENT_KEY"
        private const val ACCESS_TOKEN_KEY = "ACCESS_TOKEN_KEY"
    }

    private val viewModel by viewModel<LoginViewModel>()
    private val connectivity by inject<Connectivity>(named(LOGIN_CONNECTIVITY))
    private val parentLayout by bind<ConstraintLayout>(R.id.parent_layout_login_activity)
    private val loginErrorText by bind<TextView>(R.id.error_message_login_area)
    private val loadingView by bind<View>(R.id.loading_opaque_view_login_activity)
    private val loadingProgressBar by bind<ProgressBar>(R.id.progress_bar_loading_activity)
    private val loginButton by bind<Button>(R.id.login_button_login_area)
    private val emailInputTextArea by bind<TextInputEditText>(R.id.email_edit_text_login_area)
    private val passwordInputTextArea by bind<TextInputEditText>(R.id.password_edit_text_login_area)
    private lateinit var snackbar: Snackbar
    private lateinit var snackbarText: TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        initLoginButtonListener()
        initViewModelObservers()
        initLayoutClickListenerToHideKeyboard()
        initToolbar()
        initConnectivitySnackbar()
        initSnackbarObserver()
        initConnectivityObserver()
    }

    private fun initViewModelObservers() {
        viewModel.apply {
            getInvestor().observe(this@LoginActivity, Observer { investor ->
                navigateToCompanyCatalogActivity(investor)
                loginErrorText.gone()
            })
            userNeedsToFillInTheEmail().observe(this@LoginActivity, Observer {
                toast(getString(R.string.user_needs_to_fiil_in_the_email_login_activity))
            })
            userNeedsToFillInThePassword().observe(this@LoginActivity, Observer {
                toast(getString(R.string.user_needs_to_fill_in_the_password_login_activity))
            })
            errorRequestStatus().observe(this@LoginActivity, Observer {
                toast(getString(R.string.error_request_status_toast_message_login_activity))
            })

            unauthorizedRequestStatus().observe(this@LoginActivity, Observer {
                loginErrorText.visible()
            })

            turnOnLoadingMode().observe(this@LoginActivity, Observer {
                loadingView.visible()
                loadingProgressBar.visible()
            })
            turnOffLoadingMode().observe(this@LoginActivity, Observer {
                loadingView.gone()
                loadingProgressBar.gone()
            })
        }
    }

    private fun initLoginButtonListener() {
        loginButton.setOnClickListener {
            val email = emailInputTextArea.text ?: EMPTY_STRING
            val password = passwordInputTextArea.text ?: EMPTY_STRING
            viewModel.userWantsToLogin(email = email.toString(), password = password.toString())
        }
    }

    private fun initLayoutClickListenerToHideKeyboard() {
        this.parentLayout.onFocusChangeListener = OnFocusChangeListener { view, _ ->
            hideKeyboard(view)
        }
    }

    private fun hideKeyboard(view: View) {
        val inputMethodManager: InputMethodManager =
            getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        inputMethodManager.hideSoftInputFromWindow(view.windowToken, 0)
    }

    private fun initToolbar() {
        supportActionBar?.hide()
    }

    private fun navigateToCompanyCatalogActivity(investor: Investor) {
        val bundle = Bundle()
        bundle.putString(UID_KEY, investor.uid)
        bundle.putString(CLIENT_KEY, investor.client)
        bundle.putString(ACCESS_TOKEN_KEY, investor.accessToken)
        Navigator.navigateTo(this@LoginActivity, Navigator.Activities.COMPANY_CATALOG, bundle)
    }

    private fun initConnectivityObserver() {
        connectivity.observe(this@LoginActivity, Observer { hasNetworkConnectivity ->
            viewModel.mustShowConnectivitySnackbar(hasNetworkConnectivity = hasNetworkConnectivity)
        })
    }

    private fun initConnectivitySnackbar() {
        snackbar =
            Snackbar.make(
                this.parentLayout,
                getString(R.string.connectivity_on_snackbar),
                Snackbar.LENGTH_INDEFINITE
            )
        snackbarText = snackbar.view.findViewById(R.id.snackbar_text)
        snackbarText.textAlignment = View.TEXT_ALIGNMENT_CENTER
    }

    private fun initSnackbarObserver() {
        viewModel.apply {
            showConnectivitySnackbar().observe(this@LoginActivity, Observer {
                this@LoginActivity.showConnectivityOnSnackbar()
            })

            hideConnectivitySnackbar().observe(this@LoginActivity, Observer {
                this@LoginActivity.showConnectivityOffSnackbar()
            })
        }
    }

    private fun showConnectivityOnSnackbar() {
        snackbar.duration = Snackbar.LENGTH_LONG
        snackbar.view.setBackgroundColor(
            ContextCompat.getColor(
                this@LoginActivity,
                R.color.emerald
            )
        )
        snackbar.setText(getString(R.string.connectivity_on_snackbar))
        snackbar.show()
    }

    private fun showConnectivityOffSnackbar() {
        snackbar.duration = Snackbar.LENGTH_INDEFINITE
        snackbar.view.setBackgroundColor(
            ContextCompat.getColor(
                this@LoginActivity,
                R.color.tamarillo
            )
        )
        snackbar.setText(getString(R.string.connectivity_off_snackbar))
        snackbar.show()
    }
}
