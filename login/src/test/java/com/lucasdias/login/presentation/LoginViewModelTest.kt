package com.lucasdias.login.presentation

import androidx.lifecycle.MutableLiveData
import com.lucasdias.login.domain.model.Investor
import com.lucasdias.login.domain.model.Login
import com.lucasdias.login.domain.sealedclass.Error
import com.lucasdias.login.domain.sealedclass.Success
import com.lucasdias.login.domain.sealedclass.Unauthorized
import com.lucasdias.login.domain.usecase.PerformInvestorLoginOnTheWebServer
import io.mockk.Runs
import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.every
import io.mockk.just
import io.mockk.mockk
import io.mockk.spyk
import io.mockk.verify
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.runBlocking
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test

class LoginViewModelTest {

    private val performInvestorLoginOnTheWebServer: PerformInvestorLoginOnTheWebServer = mockk()
    private var viewModel = spyk(LoginViewModel(performInvestorLoginOnTheWebServer))

    private var coroutineContext = Dispatchers.Unconfined
    private var investor: MutableLiveData<Investor> = mockk()
    private var showConnectivitySnackbar: MutableLiveData<Unit> = mockk()
    private var hideConnectivitySnackbar: MutableLiveData<Unit> = mockk()
    private var errorRequestStatus: MutableLiveData<Unit> = mockk()
    private var unauthorizedRequestStatus: MutableLiveData<Unit> = mockk()
    private var userNeedsToFillInThePassword: MutableLiveData<Unit> = mockk()
    private var userNeedsToFillInTheEmail: MutableLiveData<Unit> = mockk()
    private var turnOnLoadingMode: MutableLiveData<Unit> = mockk()
    private var turnOffLoadingMode: MutableLiveData<Unit> = mockk()

    private val investorProfile = Investor(
        LONG_NUMBER,
        FILLED_STRING,
        FILLED_STRING,
        FILLED_STRING,
        FILLED_STRING,
        FILLED_STRING
    )

    @Before
    fun setUp() {
        viewModel.coroutineContext = coroutineContext
        viewModel.investor = investor
        viewModel.showConnectivitySnackbar = showConnectivitySnackbar
        viewModel.hideConnectivitySnackbar = hideConnectivitySnackbar
        viewModel.errorRequestStatus = errorRequestStatus
        viewModel.unauthorizedRequestStatus = unauthorizedRequestStatus
        viewModel.userNeedsToFillInThePassword = userNeedsToFillInThePassword
        viewModel.userNeedsToFillInTheEmail = userNeedsToFillInTheEmail
        viewModel.turnOnLoadingMode = turnOnLoadingMode
        viewModel.turnOffLoadingMode = turnOffLoadingMode

        every { investor.postValue(any()) } just Runs
        every { showConnectivitySnackbar.postValue(Unit) } just Runs
        every { hideConnectivitySnackbar.postValue(Unit) } just Runs
        every { errorRequestStatus.postValue(Unit) } just Runs
        every { unauthorizedRequestStatus.postValue(Unit) } just Runs
        every { userNeedsToFillInThePassword.postValue(Unit) } just Runs
        every { userNeedsToFillInTheEmail.postValue(Unit) } just Runs
        every { turnOnLoadingMode.postValue(Unit) } just Runs
        every { turnOffLoadingMode.postValue(Unit) } just Runs
    }

    @Test
    fun `IF getInvestor method was called THEN returns the investor live data`() {
        val actual = viewModel.getInvestor()
        val expected = investor
        assertEquals(expected, actual)
    }

    @Test
    fun `IF errorRequestStatus method was called THEN returns the errorRequestStatus live data`() {
        val actual = viewModel.errorRequestStatus()
        val expected = errorRequestStatus
        assertEquals(expected, actual)
    }

    @Test
    fun `IF unauthorizedRequestStatus method was called THEN returns the unauthorizedRequestStatus live data`() {
        val actual = viewModel.unauthorizedRequestStatus()
        val expected = unauthorizedRequestStatus
        assertEquals(expected, actual)
    }

    @Test
    fun `IF userNeedsToFillInThePassword method was called THEN returns the userNeedsToFillInThePassword live data`() {
        val actual = viewModel.userNeedsToFillInThePassword()
        val expected = userNeedsToFillInThePassword
        assertEquals(expected, actual)
    }

    @Test
    fun `IF userNeedsToFillInTheEmail method was called THEN returns the userNeedsToFillInTheEmail live data`() {
        val actual = viewModel.userNeedsToFillInTheEmail()
        val expected = userNeedsToFillInTheEmail
        assertEquals(expected, actual)
    }

    @Test
    fun `IF turnOnLoadingMode method was called THEN returns the turnOnLoadingMode live data`() {
        val actual = viewModel.turnOnLoadingMode()
        val expected = turnOnLoadingMode
        assertEquals(expected, actual)
    }

    @Test
    fun `IF turnOffLoadingMode method was called THEN returns the turnOffLoadingMode live data`() {
        val actual = viewModel.turnOffLoadingMode()
        val expected = turnOffLoadingMode
        assertEquals(expected, actual)
    }

    @Test
    fun `IF showConnectivitySnackbar method was called THEN returns the showConnectivitySnackbar live data`() {
        val actual = viewModel.showConnectivitySnackbar()
        val expected = showConnectivitySnackbar
        assertEquals(expected, actual)
    }

    @Test
    fun `IF hideConnectivitySnackbar method was called THEN returns the hideConnectivitySnackbar live data`() {
        val actual = viewModel.hideConnectivitySnackbar()
        val expected = hideConnectivitySnackbar
        assertEquals(expected, actual)
    }

    @Test
    fun `IF the performInvestorLoginOnTheWebServer use case method was called THEN calls the use case method`() {
        val actual = viewModel.performInvestorLoginOnTheWebServer
        val expected = performInvestorLoginOnTheWebServer
        assertEquals(expected, actual)
    }

    @Test
    fun `IF the user wants to login without internet THEN he will not login`() {
        viewModel.wasConnected = false

        every { viewModel.performInvestorLogin(any(), any()) } just Runs
        viewModel.userWantsToLogin(FILLED_STRING, FILLED_STRING)

        verify(exactly = 0) { userNeedsToFillInTheEmail.postValue(Unit) }
        verify(exactly = 0) { userNeedsToFillInThePassword.postValue(Unit) }
        verify(exactly = 0) { viewModel.performInvestorLogin(any(), any()) }
    }

    @Test
    fun `IF the user wants to login without filling in the email THEN he will not login and the app will show an message about it`() {
        viewModel.wasConnected = true

        every { viewModel.performInvestorLogin(any(), any()) } just Runs
        viewModel.userWantsToLogin(EMPTY_STRING, FILLED_STRING)

        verify(exactly = 1) { userNeedsToFillInTheEmail.postValue(Unit) }
        verify(exactly = 0) { userNeedsToFillInThePassword.postValue(Unit) }
        verify(exactly = 0) { viewModel.performInvestorLogin(any(), any()) }
    }

    @Test
    fun `IF the user wants to login without filling in the password THEN he will not login and the app will show an message about it`() {
        viewModel.wasConnected = true

        every { viewModel.performInvestorLogin(any(), any()) } just Runs
        viewModel.userWantsToLogin(FILLED_STRING, EMPTY_STRING)

        verify(exactly = 0) { userNeedsToFillInTheEmail.postValue(Unit) }
        verify(exactly = 1) { userNeedsToFillInThePassword.postValue(Unit) }
        verify(exactly = 0) { viewModel.performInvestorLogin(any(), any()) }
    }

    @Test
    fun `IF the user wants to login with internet and with all data filled in THEN he will login`() {
        viewModel.wasConnected = true

        every { viewModel.performInvestorLogin(any(), any()) } just Runs
        viewModel.userWantsToLogin(FILLED_STRING, FILLED_STRING)

        verify(exactly = 0) { userNeedsToFillInTheEmail.postValue(Unit) }
        verify(exactly = 0) { userNeedsToFillInThePassword.postValue(Unit) }
        verify(exactly = 1) { viewModel.performInvestorLogin(any(), any()) }
    }

    @Test
    fun `IF the device changes the state of the Internet from not connected to connected THEN the application will show a new message and hide the connectivity snackbar`() {
        val wasConnectedExpected = false
        val hasNetworkConnectivity = false

        viewModel.mustShowConnectivitySnackbar(hasNetworkConnectivity)

        val wasConnectedActual = viewModel.wasConnected

        verify(exactly = 1) { hideConnectivitySnackbar.postValue(Unit) }
        verify(exactly = 0) { showConnectivitySnackbar.postValue(Unit) }
        assertEquals(wasConnectedExpected, wasConnectedActual)
    }

    @Test
    fun `IF the device changes the state of the Internet from connected to not connected THEN the application will show show the connectivity snackbar`() {
        val wasConnectedExpected = true
        val hasNetworkConnectivity = true

        viewModel.wasConnected = false

        viewModel.mustShowConnectivitySnackbar(hasNetworkConnectivity)

        val wasConnectedActual = viewModel.wasConnected

        verify(exactly = 0) { hideConnectivitySnackbar.postValue(Unit) }
        verify(exactly = 1) { showConnectivitySnackbar.postValue(Unit) }
        assertEquals(wasConnectedExpected, wasConnectedActual)
    }

    @Test
    fun `IF the application calls mustShowConnectivitySnackbar but without changes the state of internet THEN the application will do nothing`() {
        val wasConnectedExpected = true
        val hasNetworkConnectivity = true

        viewModel.wasConnected = true

        viewModel.mustShowConnectivitySnackbar(hasNetworkConnectivity)

        val wasConnectedActual = viewModel.wasConnected

        verify(exactly = 0) { hideConnectivitySnackbar.postValue(Unit) }
        verify(exactly = 0) { showConnectivitySnackbar.postValue(Unit) }
        assertEquals(wasConnectedExpected, wasConnectedActual)
    }

    @Test
    fun `IF the app tries to login THEN it will call the loading live datas`() {
        val requestStatus = Success
        val login = Login(requestStatus, investorProfile)
        coEvery { viewModel.performInvestorLoginOnTheWebServer(any(), any()) } returns login

        coEvery { turnOnLoadingMode.postValue(Unit) } just Runs
        coEvery { turnOffLoadingMode.postValue(Unit) } just Runs
        coEvery { investor.postValue(any()) } just Runs
        coEvery { unauthorizedRequestStatus.postValue(Unit) } just Runs
        coEvery { errorRequestStatus.postValue(Unit) } just Runs

        runBlocking {
            viewModel.performInvestorLogin(FILLED_STRING, FILLED_STRING)
        }

        coVerify(exactly = 1) { turnOnLoadingMode.postValue(Unit) }
        coVerify(exactly = 1) { turnOffLoadingMode.postValue(Unit) }
    }

    @Test
    fun `IF the app tries to login THEN it will call the performInvestorLoginOnTheWebServer use case`() {
        val requestStatus = Success
        val login = Login(requestStatus, investorProfile)
        coEvery { viewModel.performInvestorLoginOnTheWebServer(any(), any()) } returns login

        coEvery { turnOnLoadingMode.postValue(Unit) } just Runs
        coEvery { turnOffLoadingMode.postValue(Unit) } just Runs
        coEvery { investor.postValue(any()) } just Runs
        coEvery { unauthorizedRequestStatus.postValue(Unit) } just Runs
        coEvery { errorRequestStatus.postValue(Unit) } just Runs

        runBlocking {
            viewModel.performInvestorLogin(FILLED_STRING, FILLED_STRING)
        }

        coVerify(exactly = 1) { viewModel.performInvestorLoginOnTheWebServer(any(), any()) }
    }

    @Test
    fun `IF the app login with success THEN it will post in investor live data the returned data`() {
        val requestStatus = Success
        val login = Login(requestStatus, investorProfile)
        coEvery { viewModel.performInvestorLoginOnTheWebServer(any(), any()) } returns login

        coEvery { turnOnLoadingMode.postValue(Unit) } just Runs
        coEvery { turnOffLoadingMode.postValue(Unit) } just Runs
        coEvery { investor.postValue(any()) } just Runs
        coEvery { unauthorizedRequestStatus.postValue(Unit) } just Runs
        coEvery { errorRequestStatus.postValue(Unit) } just Runs

        runBlocking {
            viewModel.performInvestorLogin(FILLED_STRING, FILLED_STRING)
        }

        coVerify(exactly = 1) { investor.postValue(any()) }
    }

    @Test
    fun `IF the app tries to login with error THEN it will call errorRequestStatus live data`() {
        val requestStatus = Error
        val login = Login(requestStatus, investorProfile)
        coEvery { viewModel.performInvestorLoginOnTheWebServer(any(), any()) } returns login

        coEvery { turnOnLoadingMode.postValue(Unit) } just Runs
        coEvery { turnOffLoadingMode.postValue(Unit) } just Runs
        coEvery { investor.postValue(any()) } just Runs
        coEvery { unauthorizedRequestStatus.postValue(Unit) } just Runs
        coEvery { errorRequestStatus.postValue(Unit) } just Runs

        runBlocking {
            viewModel.performInvestorLogin(FILLED_STRING, FILLED_STRING)
        }

        coVerify(exactly = 1) { errorRequestStatus.postValue(any()) }
    }

    @Test
    fun `IF the app tries to login with unauthorize THEN it will call unauthorizedRequestStatus live data`() {
        val requestStatus = Unauthorized
        val login = Login(requestStatus, investorProfile)
        coEvery { viewModel.performInvestorLoginOnTheWebServer(any(), any()) } returns login

        coEvery { turnOnLoadingMode.postValue(Unit) } just Runs
        coEvery { turnOffLoadingMode.postValue(Unit) } just Runs
        coEvery { investor.postValue(any()) } just Runs
        coEvery { unauthorizedRequestStatus.postValue(Unit) } just Runs
        coEvery { errorRequestStatus.postValue(Unit) } just Runs

        runBlocking {
            viewModel.performInvestorLogin(FILLED_STRING, FILLED_STRING)
        }

        coVerify(exactly = 1) { unauthorizedRequestStatus.postValue(any()) }
    }

    private companion object {
        const val LONG_NUMBER = 1L
        const val EMPTY_STRING = ""
        const val FILLED_STRING = "ABCD"
    }
}
