package com.lucasdias.companycatalog.domain.usecase

import com.lucasdias.companycatalog.domain.repository.LoginInfoRepository

class GetLoginInfoFromCache(
    private var loginInfoRepository: LoginInfoRepository
) {
    operator fun invoke(): ArrayList<Pair<String, String>> = loginInfoRepository.getLoginInfoFromCache()
}
