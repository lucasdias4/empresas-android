package com.lucasdias.companycatalog.domain.usecase

import com.lucasdias.companycatalog.domain.repository.LoginInfoRepository

class SetLoginInfoInTheCache(
    private var loginInfoRepository: LoginInfoRepository
) {
    operator fun invoke(
        client: String,
        accessToken: String,
        uid: String
    ) = loginInfoRepository.setLoginInfoInTheCache(client, accessToken, uid)
}
