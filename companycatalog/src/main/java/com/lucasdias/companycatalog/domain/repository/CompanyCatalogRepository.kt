package com.lucasdias.companycatalog.domain.repository

import com.lucasdias.companycatalog.domain.model.Company
import com.lucasdias.companycatalog.domain.sealedclass.RequestStatus

interface CompanyCatalogRepository {
    suspend fun searchCompaniesByName(
        client: String,
        accessToken: String,
        uid: String,
        name: String
    ): Pair<RequestStatus, ArrayList<Company>>
}
