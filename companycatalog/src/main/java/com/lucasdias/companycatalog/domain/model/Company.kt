package com.lucasdias.companycatalog.domain.model

data class Company(
    var id: String,
    var name: String,
    var country: String,
    var description: String,
    var photo: String?,
    var business: String
)
