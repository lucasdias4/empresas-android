package com.lucasdias.companycatalog.domain.repository

interface LoginInfoRepository {
    fun getLoginInfoFromCache(): ArrayList<Pair<String, String>>
    fun setLoginInfoInTheCache(client: String, accessToken: String, uid: String)
}
