package com.lucasdias.companycatalog.domain.sealedclass

sealed class RequestStatus

object Success : RequestStatus()
object SuccessWithoutResult : RequestStatus()
object Error : RequestStatus()
