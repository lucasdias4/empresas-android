package com.lucasdias.companycatalog.domain.usecase

import com.lucasdias.companycatalog.domain.model.Company
import com.lucasdias.companycatalog.domain.repository.CompanyCatalogRepository
import com.lucasdias.companycatalog.domain.sealedclass.AccessToken
import com.lucasdias.companycatalog.domain.sealedclass.Client
import com.lucasdias.companycatalog.domain.sealedclass.RequestStatus
import com.lucasdias.companycatalog.domain.sealedclass.Uid

class SearchCompaniesByNameFromApi(
    private var getLoginInfoFromCache: GetLoginInfoFromCache,
    private var companyCatalogRepository: CompanyCatalogRepository
) {
    suspend operator fun invoke(name: String): Pair<RequestStatus, ArrayList<Company>> {
        val headerInfo = getLoginInfoFromCache()

        val clientPair = headerInfo.first { it.first == Client.key }
        val accessTokenPair = headerInfo.first { it.first == AccessToken.key }
        val uidPair = headerInfo.first { it.first == Uid.key }

        val client = clientPair.second
        val accessToken = accessTokenPair.second
        val uid = uidPair.second

        val response = companyCatalogRepository.searchCompaniesByName(
            client,
            accessToken,
            uid,
            name
        )

        return response
    }
}
