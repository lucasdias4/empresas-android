package com.lucasdias.companycatalog.domain.sealedclass

sealed class Keys(var key: String)

object Client : Keys(key = "CLIENT_KEY")
object AccessToken : Keys(key = "ACCESS_TOKEN_KEY")
object Uid : Keys(key = "UID_KEY")
