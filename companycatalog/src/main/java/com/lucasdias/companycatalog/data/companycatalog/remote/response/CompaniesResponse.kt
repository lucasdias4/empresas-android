package com.lucasdias.companycatalog.data.companycatalog.remote.response

import com.google.gson.annotations.SerializedName

data class CompaniesResponse(
    @SerializedName("enterprises") var companies: ArrayList<CompanyResponse>
)
