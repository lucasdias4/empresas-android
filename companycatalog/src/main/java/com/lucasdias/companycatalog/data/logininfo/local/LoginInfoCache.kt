package com.lucasdias.companycatalog.data.logininfo.local

import android.content.SharedPreferences

class LoginInfoCache(private var preferences: SharedPreferences) {

    private companion object {
        private const val CLIENT_KEY = "CLIENT_KEY"
        private const val ACCESS_TOKEN_KEY = "ACCESS_TOKEN_KEY"
        private const val UID_KEY = "UID_KEY"
        private const val DEFAULT_VALUE = ""
    }

    fun setLoginInfo(client: String, accessToken: String, uid: String) {
        setClient(client)
        setAccessToken(accessToken)
        setUid(uid)
    }

    fun setClient(client: String) {
        val editor = preferences.edit()
        editor.putString(CLIENT_KEY, client)
        editor.apply()
    }

    fun setAccessToken(accessToken: String) {
        val editor = preferences.edit()
        editor.putString(ACCESS_TOKEN_KEY, accessToken)
        editor.apply()
    }

    fun setUid(uid: String) {
        val editor = preferences.edit()
        editor.putString(UID_KEY, uid)
        editor.apply()
    }

    fun getClient(): String {
        return preferences.getString(CLIENT_KEY, DEFAULT_VALUE) ?: DEFAULT_VALUE
    }

    fun getAccessToken(): String {
        return preferences.getString(ACCESS_TOKEN_KEY, DEFAULT_VALUE) ?: DEFAULT_VALUE
    }

    fun getUid(): String {
        return preferences.getString(UID_KEY, DEFAULT_VALUE) ?: DEFAULT_VALUE
    }
}
