package com.lucasdias.companycatalog.data.logininfo

import com.lucasdias.companycatalog.data.logininfo.local.LoginInfoCache
import com.lucasdias.companycatalog.domain.repository.LoginInfoRepository
import com.lucasdias.companycatalog.domain.sealedclass.AccessToken
import com.lucasdias.companycatalog.domain.sealedclass.Client
import com.lucasdias.companycatalog.domain.sealedclass.Uid

class LoginInfoRepositoryImpl(
    private val loginInfoCache: LoginInfoCache
) : LoginInfoRepository {

    override fun getLoginInfoFromCache(): ArrayList<Pair<String, String>> {
        val loginInfo = ArrayList<Pair<String, String>>()

        val client = loginInfoCache.getClient()
        val accessToken = loginInfoCache.getAccessToken()
        val uid = loginInfoCache.getUid()

        loginInfo.add(Pair(Client.key, client))
        loginInfo.add(Pair(AccessToken.key, accessToken))
        loginInfo.add(Pair(Uid.key, uid))

        return loginInfo
    }

    override fun setLoginInfoInTheCache(client: String, accessToken: String, uid: String) {
        loginInfoCache.setLoginInfo(client, accessToken, uid)
    }
}
