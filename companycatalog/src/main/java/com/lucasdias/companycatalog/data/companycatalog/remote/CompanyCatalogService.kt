package com.lucasdias.companycatalog.data.companycatalog.remote

import com.lucasdias.companycatalog.data.companycatalog.remote.response.CompaniesResponse
import kotlinx.coroutines.Deferred
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.Query

interface CompanyCatalogService {

    @GET("enterprises")
    fun searchCompaniesByName(
        @Header("client") client: String,
        @Header("access-token") accessToken: String,
        @Header("uid") uid: String,
        @Query("name") name: String
    ): Deferred<Response<CompaniesResponse>>
}
