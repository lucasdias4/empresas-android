package com.lucasdias.companycatalog.data.companycatalog.remote.response

import com.google.gson.annotations.SerializedName

data class BusinessResponse(
    @SerializedName("enterprise_type_name") val businessType: String
)
