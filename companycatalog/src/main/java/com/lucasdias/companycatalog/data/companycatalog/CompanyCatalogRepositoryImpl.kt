package com.lucasdias.companycatalog.data.companycatalog

import com.github.kittinunf.result.coroutines.SuspendableResult
import com.lucasdias.companycatalog.data.companycatalog.mapper.CompanyMapper
import com.lucasdias.companycatalog.data.companycatalog.remote.CompanyCatalogService
import com.lucasdias.companycatalog.data.companycatalog.remote.response.CompaniesResponse
import com.lucasdias.companycatalog.domain.model.Company
import com.lucasdias.companycatalog.domain.repository.CompanyCatalogRepository
import com.lucasdias.companycatalog.domain.sealedclass.Error
import com.lucasdias.companycatalog.domain.sealedclass.RequestStatus
import com.lucasdias.companycatalog.domain.sealedclass.Success
import com.lucasdias.companycatalog.domain.sealedclass.SuccessWithoutResult
import com.lucasdias.log.LogApp
import retrofit2.Response

class CompanyCatalogRepositoryImpl(
    private val companyCatalogService: CompanyCatalogService
) : CompanyCatalogRepository {

    private companion object {
        const val MIN_RESPONSE_CODE = 200
        const val MAX_RESPONSE_CODE = 299
    }

    override suspend fun searchCompaniesByName(
        client: String,
        accessToken: String,
        uid: String,
        name: String
    ): Pair<RequestStatus, ArrayList<Company>> {
        val result: SuspendableResult<Response<CompaniesResponse>, Exception> =
            SuspendableResult.of {
                companyCatalogService.searchCompaniesByName(
                    client = client,
                    accessToken = accessToken,
                    uid = uid,
                    name = name
                ).await()
            }

        val domainCompanies = ArrayList<Company>()
        val resultCode = result.component1()?.code()
        val resultException = result.component2()
        val companiesResponse = result.component1()?.body()
        val status = resultStatusHandler(
            resultCode = resultCode,
            resultException = resultException,
            companiesResponse = companiesResponse
        )

        when (status) {
            Success -> {
                val mappedCompanies = CompanyMapper.map(companiesResponse)
                domainCompanies.addAll(mappedCompanies)
                logRequestInfo(companiesResponse = companiesResponse, searchText = name)
            }

            SuccessWithoutResult -> {
            }

            Error -> {
                val exception = result.component2()
                logRequestException(exception = exception, resultCode = resultCode)
            }
        }

        return Pair(status, domainCompanies)
    }

    private fun resultStatusHandler(
        resultCode: Int?,
        resultException: java.lang.Exception?,
        companiesResponse: CompaniesResponse?
    ): RequestStatus {
        if (resultCode in MIN_RESPONSE_CODE..MAX_RESPONSE_CODE) {

            val searchWithoutResult = companiesResponse?.companies.isNullOrEmpty()
            val isAnException = resultException != null

            if (searchWithoutResult) return SuccessWithoutResult
            if (isAnException) return Error
            return Success
        } else {
            return Error
        }
    }

    private fun logRequestInfo(
        companiesResponse: CompaniesResponse?,
        searchText: String
    ) {
        LogApp.i("CompanyCatalog", "Request response START ---------->")
        LogApp.i("CompanyCatalog", "search for: $searchText")
        companiesResponse?.companies?.forEach { company ->
            LogApp.i(
                "CompanyCatalog", " \nCompany id: ${company.id}" +
                        "\nName: ${company.name}" +
                        "\nPhoto: ${company.photo}" +
                        "\nCountry: ${company.country}" +
                        "\nDescription: ${company.description}"
            )
        }
        LogApp.i("CompanyCatalog", "Request response END <----------")
    }

    private fun logRequestException(exception: java.lang.Exception?, resultCode: Int?) {
        LogApp.i("CompanyCatalog", "Request exception START ---------->")
        LogApp.i("CompanyCatalog", "Exception or ErrorCode: ${exception ?: resultCode}")
        LogApp.i("CompanyCatalog", "Request exception END <----------")
    }
}
