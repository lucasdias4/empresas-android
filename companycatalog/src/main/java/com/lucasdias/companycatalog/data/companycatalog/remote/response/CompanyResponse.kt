package com.lucasdias.companycatalog.data.companycatalog.remote.response

import com.google.gson.annotations.SerializedName

data class CompanyResponse(
    val id: String,
    @SerializedName("enterprise_name") val name: String,
    val country: String,
    val description: String,
    val photo: String?,
    @SerializedName("enterprise_type") val business: BusinessResponse
)
