package com.lucasdias.companycatalog.data.companycatalog.mapper

import com.lucasdias.companycatalog.data.companycatalog.remote.response.CompaniesResponse
import com.lucasdias.companycatalog.domain.model.Company

class CompanyMapper {
    companion object {

        fun map(companiesResponse: CompaniesResponse?): ArrayList<Company> {
            val companies = ArrayList<Company>()

            companiesResponse?.companies?.forEach { companyResponse ->
                val company = Company(
                    id = companyResponse.id,
                    name = companyResponse.name,
                    country = companyResponse.country,
                    photo = companyResponse.photo,
                    description = companyResponse.description,
                    business = companyResponse.business.businessType
                )
                companies.add(company)
            }
            return companies
        }
    }
}
