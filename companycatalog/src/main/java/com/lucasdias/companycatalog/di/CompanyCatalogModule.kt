package com.lucasdias.companycatalog.di

import android.app.Activity
import android.content.Context
import android.content.SharedPreferences
import android.widget.FrameLayout
import androidx.appcompat.app.ActionBar
import androidx.preference.PreferenceManager
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import com.lucasdias.companycatalog.BuildConfig.API_URL
import com.lucasdias.companycatalog.data.companycatalog.CompanyCatalogRepositoryImpl
import com.lucasdias.companycatalog.data.companycatalog.remote.CompanyCatalogService
import com.lucasdias.companycatalog.data.logininfo.LoginInfoRepositoryImpl
import com.lucasdias.companycatalog.data.logininfo.local.LoginInfoCache
import com.lucasdias.companycatalog.domain.model.Company
import com.lucasdias.companycatalog.domain.repository.CompanyCatalogRepository
import com.lucasdias.companycatalog.domain.repository.LoginInfoRepository
import com.lucasdias.companycatalog.domain.usecase.GetLoginInfoFromCache
import com.lucasdias.companycatalog.domain.usecase.SearchCompaniesByNameFromApi
import com.lucasdias.companycatalog.domain.usecase.SetLoginInfoInTheCache
import com.lucasdias.companycatalog.presentation.CompanyCatalogAdapter
import com.lucasdias.companycatalog.presentation.CompanyCatalogViewModel
import com.lucasdias.connectivity.Connectivity
import com.lucasdias.toolbar.ToolbarController
import java.util.concurrent.TimeUnit
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.android.ext.koin.androidApplication
import org.koin.android.ext.koin.androidContext
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.core.qualifier.named
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

const val COMPANY_CATALOG_CONNECTIVITY = "COMPANY_CATALOG_CONNECTIVITY"
const val COMPANY_CATALOG_TOOLBAR_CONTROLLER = "COMPANY_CATALOG_TOOLBAR_CONTROLLER"
private const val COMPANY_CATALOG_RETROFIT = "COMPANY_CATALOG_RETROFIT"
private const val COMPANY_CATALOG_OKHTTP = "COMPANY_CATALOG_OKHTTP"

@Suppress("RemoveExplicitTypeArguments", "USELESS_CAST")
val companyCatalogModule = module {
    viewModel {
        CompanyCatalogViewModel(
            get<SetLoginInfoInTheCache>(),
            get<SearchCompaniesByNameFromApi>()
        )
    }

    factory { (method: ((Company) -> Unit)?) ->
        CompanyCatalogAdapter(method)
    }

    factory(named(COMPANY_CATALOG_TOOLBAR_CONTROLLER)) { (activity: Activity, toolbarContainer: FrameLayout, actionBar: ActionBar) ->
        ToolbarController(
            activity = activity,
            toolbarContainer = toolbarContainer,
            supportActionBar = actionBar
        )
    }

    factory(named(COMPANY_CATALOG_CONNECTIVITY)) {
        Connectivity(
            application = androidApplication()
        )
    }

    factory {
        GetLoginInfoFromCache(get<LoginInfoRepository>())
    }

    factory {
        SetLoginInfoInTheCache(get<LoginInfoRepository>())
    }

    factory {
        SearchCompaniesByNameFromApi(
            get<GetLoginInfoFromCache>(),
            get<CompanyCatalogRepository>()
        )
    }

    factory {
        LoginInfoRepositoryImpl(
            get<LoginInfoCache>()
        ) as LoginInfoRepository
    }

    factory {
        CompanyCatalogRepositoryImpl(
            get<CompanyCatalogService>()
        ) as CompanyCatalogRepository
    }

    // Persistence
    factory {
        LoginInfoCache(
            get<SharedPreferences>()
        )
    }

    single {
        getSharedPreferences(context = androidContext())
    }

    // Service
    // Service
    factory(named(COMPANY_CATALOG_OKHTTP)) {
        createOkHttpClient()
    }

    single(named(COMPANY_CATALOG_RETROFIT)) {
        createRetrofit(
            get<OkHttpClient>(named(COMPANY_CATALOG_OKHTTP))
        )
    }

    factory {
        getCompanyCatalogService(
            get<Retrofit>(named(COMPANY_CATALOG_RETROFIT))
        )
    }
}

private fun getSharedPreferences(context: Context): SharedPreferences {
    val preferences = PreferenceManager.getDefaultSharedPreferences(context)
    return preferences
}

private fun createOkHttpClient(): OkHttpClient {
    val timeoutInSeconds = 10L
    val httpLoggingInterceptor = HttpLoggingInterceptor()
    httpLoggingInterceptor.level = HttpLoggingInterceptor.Level.BASIC
    return OkHttpClient.Builder()
        .connectTimeout(timeoutInSeconds, TimeUnit.SECONDS)
        .readTimeout(timeoutInSeconds, TimeUnit.SECONDS)
        .addInterceptor(httpLoggingInterceptor).build()
}

private fun createRetrofit(okHttpClient: OkHttpClient): Retrofit = Retrofit.Builder()
    .baseUrl(API_URL)
    .client(okHttpClient)
    .addConverterFactory(GsonConverterFactory.create())
    .addCallAdapterFactory(CoroutineCallAdapterFactory())
    .build()

private fun getCompanyCatalogService(retrofit: Retrofit): CompanyCatalogService =
    retrofit.create(CompanyCatalogService::class.java)
