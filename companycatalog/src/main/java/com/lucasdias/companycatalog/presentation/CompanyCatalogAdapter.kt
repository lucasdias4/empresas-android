package com.lucasdias.companycatalog.presentation

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.lucasdias.companycatalog.R
import com.lucasdias.companycatalog.domain.model.Company
import com.lucasdias.extensions.bind
import com.lucasdias.extensions.loadUrl

class CompanyCatalogAdapter(private val navigateMethod: ((Company) -> Unit)?) :
    RecyclerView.Adapter<CompanyCatalogAdapter.ViewHolder>() {

    private var companies = mutableListOf<Company>()

    fun updateCompanyCatalog(companies: ArrayList<Company>) {
        this.companies.clear()

        this.companies.addAll(companies)
        notifyDataSetChanged()
    }

    fun clearCompanyCatalog() {
        this.companies.clear()
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val viewLayout =
            LayoutInflater.from(parent.context)
                .inflate(R.layout.company_list_item, parent, false)
        return ViewHolder(viewLayout)
    }

    override fun getItemCount(): Int {
        return companies.count()
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        if (companies.isNotEmpty()) {
            holder.bind(company = companies[position], navigateMethod = navigateMethod)
        }
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        private val name by bind<TextView>(itemView, R.id.name_company_list_item)
        private val business by bind<TextView>(itemView, R.id.business_company_list_item)
        private val country by bind<TextView>(itemView, R.id.country_company_list_item)
        private val picture by bind<ImageView>(itemView, R.id.picture_company_list_item)

        fun bind(
            company: Company,
            navigateMethod: ((Company) -> Unit)?
        ) {
            name.text = company.name
            business.text = company.business
            country.text = company.country
            picture.loadUrl(url = company.photo, placeHolderId = R.drawable.company_picture)

            itemView.setOnClickListener {
                navigateMethod?.let { method ->
                    method(company)
                }
            }
        }
    }
}
