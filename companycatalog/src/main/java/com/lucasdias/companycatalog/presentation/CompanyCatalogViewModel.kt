package com.lucasdias.companycatalog.presentation

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.lucasdias.companycatalog.domain.model.Company
import com.lucasdias.companycatalog.domain.sealedclass.Error
import com.lucasdias.companycatalog.domain.sealedclass.RequestStatus
import com.lucasdias.companycatalog.domain.sealedclass.Success
import com.lucasdias.companycatalog.domain.sealedclass.SuccessWithoutResult
import com.lucasdias.companycatalog.domain.usecase.GetLoginInfoFromCache
import com.lucasdias.companycatalog.domain.usecase.SearchCompaniesByNameFromApi
import com.lucasdias.companycatalog.domain.usecase.SetLoginInfoInTheCache
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class CompanyCatalogViewModel(
    private val setLoginInfoInTheCache: SetLoginInfoInTheCache,
    private val searchCompaniesByNameFromApi: SearchCompaniesByNameFromApi
) : ViewModel() {

    private var coroutineContext = Dispatchers.IO

    private var wasConnected = true
    private var showConnectivitySnackbar = MutableLiveData<Unit>()
    private var hideConnectivitySnackbar = MutableLiveData<Unit>()
    private var companies = MutableLiveData<ArrayList<Company>>()
    private var emptySearchRequestStatus = MutableLiveData<Unit>()
    private var errorRequestStatus = MutableLiveData<Unit>()
    private var userNeedsToFillSearch = MutableLiveData<Unit>()
    private var turnOnLoadingMode = MutableLiveData<Unit>()
    private var turnOffLoadingMode = MutableLiveData<Unit>()

    fun getCompanies(): LiveData<ArrayList<Company>> = companies
    fun emptySearchRequestStatus(): LiveData<Unit> = emptySearchRequestStatus
    fun errorRequestStatus(): LiveData<Unit> = errorRequestStatus
    fun userNeedsToFillSearch(): LiveData<Unit> = userNeedsToFillSearch
    fun turnOnLoadingMode(): LiveData<Unit> = turnOnLoadingMode
    fun turnOffLoadingMode(): LiveData<Unit> = turnOffLoadingMode
    fun showConnectivitySnackbar(): LiveData<Unit> = showConnectivitySnackbar
    fun hideConnectivitySnackbar(): LiveData<Unit> = hideConnectivitySnackbar

    fun setLoginInfo(client: String, accessToken: String, uid: String) {
        setLoginInfoInTheCache(client, accessToken, uid)
    }

    fun searchCompaniesByName(name: String) {
        if (wasConnected.not()) return

        if (name.isEmpty()) {
            userNeedsToFillSearch.postValue(Unit)
            return
        }

        CoroutineScope(coroutineContext).launch {
            turnOnLoadingMode.postValue(Unit)
            val response = searchCompaniesByNameFromApi(name)
            searchCompaniesResponseHandler(response)
            turnOffLoadingMode.postValue(Unit)
        }
    }

    private fun searchCompaniesResponseHandler(response: Pair<RequestStatus, java.util.ArrayList<Company>>) {
        val companyList = response.second
        val requestStatus = response.first
        when (requestStatus) {
            Success -> companies.postValue(companyList)
            SuccessWithoutResult -> emptySearchRequestStatus.postValue(Unit)
            Error -> errorRequestStatus.postValue(Unit)
        }
    }

    fun mustShowConnectivitySnackbar(hasNetworkConnectivity: Boolean) {
        if (hasNetworkConnectivity.not()) {
            hideConnectivitySnackbar.postValue(Unit)
            wasConnected = false
        } else if (wasConnected.not() && hasNetworkConnectivity) {
            showConnectivitySnackbar.postValue(Unit)
            wasConnected = true
        }
    }
}
