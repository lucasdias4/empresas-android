package com.lucasdias.companycatalog.presentation

import android.os.Bundle
import android.view.View
import android.widget.FrameLayout
import android.widget.ProgressBar
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.snackbar.Snackbar
import com.lucasdias.companycatalog.R
import com.lucasdias.companycatalog.di.COMPANY_CATALOG_CONNECTIVITY
import com.lucasdias.companycatalog.di.COMPANY_CATALOG_TOOLBAR_CONTROLLER
import com.lucasdias.companycatalog.domain.model.Company
import com.lucasdias.connectivity.Connectivity
import com.lucasdias.extensions.bind
import com.lucasdias.extensions.gone
import com.lucasdias.extensions.toast
import com.lucasdias.extensions.visible
import com.lucasdias.navigator.Navigator
import com.lucasdias.toolbar.ToolbarController
import org.koin.android.ext.android.inject
import org.koin.android.viewmodel.ext.android.viewModel
import org.koin.core.parameter.parametersOf
import org.koin.core.qualifier.named

class CompanyCatalogActivity : AppCompatActivity() {

    private companion object {
        private const val CLIENT_KEY = "CLIENT_KEY"
        private const val ACCESS_TOKEN_KEY = "ACCESS_TOKEN_KEY"
        private const val UID_KEY = "UID_KEY"
        private const val PICTURE_KEY = "PICTURE_KEY"
        private const val DESCRIPTION_KEY = "DESCRIPTION_KEY"
        private const val EMPTY_STRING = ""
    }

    private val viewModel by viewModel<CompanyCatalogViewModel>()
    private val adapter by inject<CompanyCatalogAdapter> {
        parametersOf(navigateToCompanyDetailMethod)
    }
    private val toolbarController by inject<ToolbarController>(
        named(COMPANY_CATALOG_TOOLBAR_CONTROLLER)
    ) {
        parametersOf(this, toolbarContainer, supportActionBar)
    }
    private val connectivity by inject<Connectivity>(named(COMPANY_CATALOG_CONNECTIVITY))
    private val parentLayout by bind<ConstraintLayout>(R.id.parent_company_catalog_activity)
    private val toolbarContainer by bind<FrameLayout>(R.id.toolbar_container_company_catalog_activity)
    private val recyclerView by bind<RecyclerView>(R.id.companies_recycler_view_company_catalog_activity)
    private val loadingOpaqueView by bind<View>(R.id.loading_opaque_view_company_catalog_activity)
    private val welcomeMessage by bind<TextView>(R.id.welcome_message_company_catalog_activity)
    private val emptySearchMessage by bind<TextView>(R.id.empty_search_message_company_catalog_activity)
    private val progressBar by bind<ProgressBar>(R.id.progress_bar_company_catalog_activity)
    private val navigateToCompanyDetailMethod = { company: Company ->
        navigateToCompanyDetail(company)
    }
    private val doASearchMethod = { searchText: String ->
        viewModel.searchCompaniesByName(searchText)
    }
    private lateinit var layoutManager: LinearLayoutManager
    private lateinit var snackbar: Snackbar
    private lateinit var snackbarText: TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_company_catalog)

        setLoginInfoInTheCache()
        initViewModelObservers()
        initRecyclerView()
        initToolbar()
        initConnectivitySnackbar()
        initSnackbarObserver()
        initConnectivityObserver()
    }

    private fun initToolbar() {
        val toolbarColor = R.color.rose_pearl
        val toolbarFrontView = layoutInflater.inflate(R.layout.toolbar_front_view, null)
        val toolbarTextInputHintColor = R.color.tamarillo
        val toolbarTextInputLineColor = R.color.white
        val toolbarTextInputTextColor = R.color.white
        val toolbarTextInputHintText =
            getString(R.string.toolbar_hint_text_company_catalog_activity)
        val toolbarTextInputLefDrawable = R.drawable.search_icon

        toolbarController.setToolbarColor(toolbarColor)

        toolbarController.setToolbarFrontView(toolbarFrontView)

        toolbarController.setTextInput(doASearchMethod)
        toolbarController.setTextInputHintColor(toolbarTextInputHintColor)
        toolbarController.setTextInputHintText(toolbarTextInputHintText)
        toolbarController.setTextInputLeftIcon(toolbarTextInputLefDrawable)
        toolbarController.setTextInputLineColor(toolbarTextInputLineColor)
        toolbarController.setTextInputTextColor(toolbarTextInputTextColor)
    }

    private fun setLoginInfoInTheCache() {
        val client = intent.getStringExtra(CLIENT_KEY) ?: EMPTY_STRING
        val accessToken = intent.getStringExtra(ACCESS_TOKEN_KEY) ?: EMPTY_STRING
        val uid = intent.getStringExtra(UID_KEY) ?: EMPTY_STRING

        viewModel.setLoginInfo(client, accessToken, uid)
    }

    private fun initRecyclerView() {
        layoutManager =
            LinearLayoutManager(this@CompanyCatalogActivity, LinearLayoutManager.VERTICAL, false)
        recyclerView.setHasFixedSize(true)
        recyclerView.layoutManager = layoutManager
        recyclerView.adapter = adapter
    }

    private fun initViewModelObservers() {
        viewModel.apply {
            getCompanies().observe(this@CompanyCatalogActivity, Observer { companies ->
                emptySearchMessage.gone()
                adapter.updateCompanyCatalog(companies)
            })
            emptySearchRequestStatus().observe(this@CompanyCatalogActivity, Observer {
                adapter.clearCompanyCatalog()
                emptySearchMessage.visible()
            })
            errorRequestStatus().observe(this@CompanyCatalogActivity, Observer {
                adapter.clearCompanyCatalog()
                emptySearchMessage.gone()
                toast(getString(R.string.error_request_status_status_toast_message_company_catalog_activity))
            })
            userNeedsToFillSearch().observe(this@CompanyCatalogActivity, Observer {
                toast(getString(R.string.user_needs_to_fill_search_toast_message_company_catalog_activity))
            })
            turnOnLoadingMode().observe(this@CompanyCatalogActivity, Observer {
                adapter.clearCompanyCatalog()
                emptySearchMessage.gone()
                loadingOpaqueView.visible()
                progressBar.visible()
            })
            turnOffLoadingMode().observe(this@CompanyCatalogActivity, Observer {
                loadingOpaqueView.gone()
                progressBar.gone()
                welcomeMessage.gone()
            })
        }
    }

    private fun navigateToCompanyDetail(company: Company) {
        val bundle = Bundle()
        bundle.putString(PICTURE_KEY, company.photo)
        bundle.putString(DESCRIPTION_KEY, company.description)
        Navigator.navigateTo(
            this@CompanyCatalogActivity,
            Navigator.Activities.COMPANY_DETAIL,
            bundle
        )
    }

    private fun initConnectivityObserver() {
        connectivity.observe(this@CompanyCatalogActivity, Observer { hasNetworkConnectivity ->
            viewModel.mustShowConnectivitySnackbar(hasNetworkConnectivity = hasNetworkConnectivity)
        })
    }

    private fun initConnectivitySnackbar() {
        snackbar =
            Snackbar.make(
                parentLayout,
                getString(R.string.connectivity_on_snackbar),
                Snackbar.LENGTH_INDEFINITE
            )
        snackbarText = snackbar.view.findViewById(R.id.snackbar_text)
        snackbarText.textAlignment = View.TEXT_ALIGNMENT_CENTER
    }

    private fun initSnackbarObserver() {
        viewModel.apply {
            showConnectivitySnackbar().observe(this@CompanyCatalogActivity, Observer {
                this@CompanyCatalogActivity.showConnectivityOnSnackbar()
            })

            hideConnectivitySnackbar().observe(this@CompanyCatalogActivity, Observer {
                this@CompanyCatalogActivity.showConnectivityOffSnackbar()
            })
        }
    }

    private fun showConnectivityOnSnackbar() {
        snackbar.duration = Snackbar.LENGTH_LONG
        snackbar.view.setBackgroundColor(
            ContextCompat.getColor(
                this@CompanyCatalogActivity,
                R.color.emerald
            )
        )
        snackbar.setText(getString(R.string.connectivity_on_snackbar))
        snackbar.show()
    }

    private fun showConnectivityOffSnackbar() {
        snackbar.duration = Snackbar.LENGTH_INDEFINITE
        snackbar.view.setBackgroundColor(
            ContextCompat.getColor(
                this@CompanyCatalogActivity,
                R.color.tamarillo
            )
        )
        snackbar.setText(getString(R.string.connectivity_off_snackbar))
        snackbar.show()
    }
}
