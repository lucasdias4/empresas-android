package com.lucasdias.companydetail.di

import android.app.Activity
import android.widget.FrameLayout
import androidx.appcompat.app.ActionBar
import com.lucasdias.toolbar.ToolbarController
import org.koin.core.qualifier.named
import org.koin.dsl.module

const val COMPANY_DETAIL_TOOLBAR_CONTROLLER = "COMPANY_DETAIL_TOOLBAR_CONTROLLER"

@Suppress("RemoveExplicitTypeArguments", "USELESS_CAST")
val companyDetailModule = module {
    factory(named(COMPANY_DETAIL_TOOLBAR_CONTROLLER)) { (activity: Activity, toolbarContainer: FrameLayout, actionBar: ActionBar) ->
        ToolbarController(
            activity = activity,
            toolbarContainer = toolbarContainer,
            supportActionBar = actionBar
        )
    }
}
