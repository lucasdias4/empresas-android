package com.lucasdias.companydetail.presentation

import android.os.Bundle
import android.widget.FrameLayout
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import com.lucasdias.companydetail.R
import com.lucasdias.companydetail.di.COMPANY_DETAIL_TOOLBAR_CONTROLLER
import com.lucasdias.extensions.bind
import com.lucasdias.extensions.loadUrl
import com.lucasdias.toolbar.ToolbarController
import org.koin.android.ext.android.inject
import org.koin.core.parameter.parametersOf
import org.koin.core.qualifier.named

class CompanyDetailActivity : AppCompatActivity() {

    private companion object {
        const val PICTURE_KEY = "PICTURE_KEY"
        const val DESCRIPTION_KEY = "DESCRIPTION_KEY"
        const val EMPTY_STRING = ""
    }

    private val toolbarController by inject<ToolbarController>(
        named(COMPANY_DETAIL_TOOLBAR_CONTROLLER)
    ) { parametersOf(this, toolbarContainer, supportActionBar) }
    private val toolbarContainer by bind<FrameLayout>(R.id.toolbar_container_company_detail_activity)
    private val picture by bind<ImageView>(R.id.picture_company_detail_activity)
    private val description by bind<TextView>(R.id.description_company_detail_activity)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_company_detail)

        initToolbar()

        val descriptionText = intent.getStringExtra(DESCRIPTION_KEY) ?: EMPTY_STRING
        val pictureUrl = intent.getStringExtra(PICTURE_KEY) ?: EMPTY_STRING

        description.text = descriptionText
        picture.loadUrl(url = pictureUrl, placeHolderId = R.drawable.company_picture)
    }

    private fun initToolbar() {
        val toolbarColor = R.color.rose_pearl
        toolbarController.setToolbarColor(toolbarColor)
        toolbarController.setToolbarBackButton()
    }
}
